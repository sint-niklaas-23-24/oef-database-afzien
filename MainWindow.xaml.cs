﻿using System;
using System.Collections.Generic;
using System.Windows;


namespace SpeedrunLokaalAny_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //Global variabeles & lists
        List<Lokaal> lijstHuidigeLokalen;
        List<Vak> lijstHuidigeVakken;
        List<Leerling> lijstHuidigeLeerlingen;
        List<Leerkracht> lijstHuidigeLeerkrachten;
        Lokaal selecteerdeLokaal = new Lokaal();
        Vak selecteerdeVak = new Vak();
        Leerkracht selecteerdeLeerkracht = new Leerkracht();
        Leerling selecteerdeLeerling = new Leerling();
        LeerlingVak selecteerdeLeerlingVak = new LeerlingVak();
        //Window Load
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            LeesAllEnRefresh();
        }


        //Lokalen
        private void btnToevoegen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selecteerdeLokaal == null)
                {
                    selecteerdeLokaal = new Lokaal();
                }
                if (selecteerdeVak == null)
                {
                    selecteerdeVak = new Vak();
                }
                selecteerdeLokaal.LokaalCode = txtLokaalcode.Text;
                selecteerdeLokaal.Startuur = dpStart.Value;
                selecteerdeLokaal.Einduur = dpEind.Value;
                selecteerdeVak = cmbVakken.SelectedItem as Vak;
                if (selecteerdeVak == null)
                {
                    selecteerdeLokaal.FKVak = null;
                }
                else
                {
                    selecteerdeLokaal.FKVak = selecteerdeVak.VakID;


                }
                Datamanager.InsertLokaal(selecteerdeLokaal);
                LeesAllEnRefresh();

            }
            catch { MessageBox.Show("Er ging iets mis! Dubbelcheck de input!"); }
        }
        private void btnLeesEen_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (int.TryParse(txtRecordnummer.Text, out int result))
                {
                    if (Datamanager.GetLokaal(Convert.ToInt32(txtRecordnummer.Text)) != null)
                    {

                        lbDisplay.ItemsSource = Datamanager.GetLokaal(Convert.ToInt32(txtRecordnummer.Text)).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Record werd niet gevonden in de database.");
                    }
                }
                else
                {
                    MessageBox.Show("Foutieve waarde! Geen geheel cijfer was ingevoerd.");
                }
            }
            catch { MessageBox.Show("Er ging iets mis! Dubbelcheck de input!"); }
        }
        private void btnLeesAlle_Click(object sender, RoutedEventArgs e)
        {
            LeesAllEnRefresh();
        }
        private void btnUpdate_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLokaal = lbDisplay.SelectedItem as Lokaal;
                selecteerdeLokaal.LokaalCode = txtLokaalcode.Text;
                selecteerdeLokaal.Startuur = dpStart.Value;
                selecteerdeLokaal.Einduur = dpEind.Value;
                selecteerdeVak = cmbVakken.SelectedItem as Vak;
                if (selecteerdeVak == null)
                {
                    selecteerdeLokaal.FKVak = null;
                }
                else
                {
                    selecteerdeLokaal.FKVak = selecteerdeVak.VakID;

                }
                if (Datamanager.UpdateLokaal(selecteerdeLokaal))
                {
                    MessageBox.Show("Update succesvol!");
                }

                LeesAllEnRefresh();

            }
            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void btnDelete_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLokaal = lbDisplay.SelectedItem as Lokaal;
                Datamanager.DeleteLokaal(selecteerdeLokaal);
                LeesAllEnRefresh();

            }

            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void lbDisplay_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

            selecteerdeLokaal = lbDisplay.SelectedItem as Lokaal;
            if (lbDisplay.SelectedItem != null)
            {
                VeldenInvullen();
            }
            else
            {
                ResetVelden();
            }
        }


        //Vakken
        private void btnToevoegenVak_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selecteerdeLeerkracht == null)
                {
                    selecteerdeLeerkracht = new Leerkracht();

                }
                if (selecteerdeVak == null)
                {
                    selecteerdeVak = new Vak();

                }
                selecteerdeVak.Naam = txtVaknaam.Text;
                selecteerdeLeerkracht = cmbLeerkracht.SelectedItem as Leerkracht;

                if (selecteerdeLeerkracht == null)
                {
                    selecteerdeVak.FKLeerkracht = null;
                }
                else
                {
                    selecteerdeVak.FKLeerkracht = selecteerdeLeerkracht.LeerkrachtID;

                }
                Datamanager.InsertVak(selecteerdeVak);
                LeesAllEnRefresh();

            }
            catch { MessageBox.Show("Er ging iets mis! Dubbelcheck de input!"); }
        }
        private void btnUpdateVak_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeVak = lbDisplayVakken.SelectedItem as Vak;
                selecteerdeLeerkracht = cmbLeerkracht.SelectedItem as Leerkracht;
                selecteerdeVak.Naam = txtVaknaam.Text;
                if (selecteerdeLeerkracht == null)
                {
                    selecteerdeVak.FKLeerkracht = null;
                }
                else
                {
                    selecteerdeVak.FKLeerkracht = selecteerdeLeerkracht.LeerkrachtID;

                }
                if (Datamanager.UpdateVak(selecteerdeVak))
                {
                    MessageBox.Show("Update succesvol!");
                }
                LeesAllEnRefresh();
            }
            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void btnDeleteVak_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                selecteerdeVak = lbDisplayVakken.SelectedItem as Vak;
                Datamanager.DeleteVak(selecteerdeVak);
                LeesAllEnRefresh();
            }

            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void lbDisplayVakken_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

            selecteerdeVak = lbDisplayVakken.SelectedItem as Vak;
            if (lbDisplayVakken.SelectedItem != null)
            {
                txtVaknaam.Text = selecteerdeVak.Naam;
            }
            else
            {
                txtVaknaam.Clear();
            }
        }


        //Leerlingen
        private void btnToevoegenLln_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selecteerdeLeerling == null)           //Vermijdt errors wanneer je delete > add (delete maakt selecteerdeleerling null)
                {
                    selecteerdeLeerling = new Leerling();
                }

                selecteerdeLeerling.Naam = txtLlnNaam.Text;
                selecteerdeLeerling.Voornaam = txtLlnVoornaam.Text;

                // Vak toevoegen aan deze leerling van de tussentabel
                Datamanager.InsertLeerling(selecteerdeLeerling);
                LeesAllEnRefresh();

            }
            catch { MessageBox.Show("Er ging iets mis! Dubbelcheck de input!"); }
        }
        private void btnVerwijderenLln_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLeerling = lbDisplayLln.SelectedItem as Leerling;
                Datamanager.DeleteLeerling(selecteerdeLeerling);
                LeesAllEnRefresh();
            }

            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void btnUpdateLln_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLeerling = lbDisplayLln.SelectedItem as Leerling;
                selecteerdeLeerling.Naam = txtLlnNaam.Text;
                selecteerdeLeerling.Voornaam = txtLlnVoornaam.Text;

                if (Datamanager.UpdateLeerling(selecteerdeLeerling))
                {
                    MessageBox.Show("Update succesvol!");
                }
                LeesAllEnRefresh();
            }
            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void lbDisplayLln_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            selecteerdeLeerling = (Leerling)lbDisplayLln.SelectedItem;
            if (selecteerdeLeerling != null)
            {
                lbDisplayVakkenTwee.ItemsSource = null;
                lbDisplayVakkenTwee.ItemsSource = Datamanager.AlleVakkenLeerling(selecteerdeLeerling.LeerlingID);
            }

            if (lbDisplayLln != null && selecteerdeLeerling != null)
            {
                txtLlnVoornaam.Text = selecteerdeLeerling.Voornaam;
                txtLlnNaam.Text = selecteerdeLeerling.Naam;
            }
            else
            {
                txtLlnVoornaam.Clear();
                txtLlnNaam.Clear();
            }
        }
        private void btnVakkenWeergeven_Click(object sender, RoutedEventArgs e)
        {
            cmbVakkenTwee.ItemsSource = null;
            cmbVakkenTwee.ItemsSource = lijstHuidigeVakken;
        }

        //VAKKENCRUD IN LLN
        private void btnVoegVakLln_Click(object sender, RoutedEventArgs e)
        {
            if (selecteerdeLeerling == null)           //Vermijdt errors wanneer je delete > add (delete maakt selecteerdeleerling null)
            {
                selecteerdeLeerling = new Leerling();
            }
            if (selecteerdeVak != null)
            {
                selecteerdeVak = cmbVakkenTwee.SelectedItem as Vak;
            }

            selecteerdeLeerlingVak.FKVak = selecteerdeVak.VakID;
            selecteerdeLeerlingVak.FKLeerling = selecteerdeLeerling.LeerlingID;
            Datamanager.InsertLeerlingVak(selecteerdeLeerlingVak);
            LeesAllEnRefresh();
        }
        private void btnVerwijderVakLln_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLeerlingVak = lbDisplayVakkenTwee.SelectedItem as LeerlingVak;
                Datamanager.DeleteLeerlingVak(selecteerdeLeerlingVak);
                LeesAllEnRefresh();
            }
            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }

        }
        private void btnUpdateVakLln_Click(object sender, RoutedEventArgs e)
        {
            selecteerdeLeerlingVak = lbDisplayVakkenTwee.SelectedItem as LeerlingVak;
            selecteerdeLeerling = lbDisplayLln.SelectedItem as Leerling;
            selecteerdeVak = cmbVakkenTwee.SelectedItem as Vak;
            selecteerdeLeerlingVak.FKVak = selecteerdeVak.VakID;
            selecteerdeLeerlingVak.FKLeerling = selecteerdeLeerling.LeerlingID;
            Datamanager.UpdateLeerlingVak(selecteerdeLeerlingVak);
            LeesAllEnRefresh();
        }

        //Leerkrachten
        private void btnToevoegenLk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (selecteerdeLeerkracht == null)
                {
                    selecteerdeLeerkracht = new Leerkracht();
                }
                selecteerdeLeerkracht.Naam = txtLkNaam.Text;
                selecteerdeLeerkracht.Voornaam = txtLkVoornaam.Text;
                Datamanager.InsertLeerkracht(selecteerdeLeerkracht);
                LeesAllEnRefresh();

            }
            catch { MessageBox.Show("Er ging iets mis! Dubbelcheck de input!"); }
        }
        private void btnVerwijderenLk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLeerkracht = lbDisplayLln.SelectedItem as Leerkracht;
                Datamanager.DeleteLeerkracht(selecteerdeLeerkracht);
                LeesAllEnRefresh();
            }

            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void btnUpdateLk_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                selecteerdeLeerkracht = lbDisplayLk.SelectedItem as Leerkracht;
                selecteerdeLeerkracht.Naam = txtLkNaam.Text;
                selecteerdeLeerkracht.Voornaam = txtLkVoornaam.Text;

                if (Datamanager.UpdateLeerkracht(selecteerdeLeerkracht))
                {
                    MessageBox.Show("Update succesvol!");
                }
                LeesAllEnRefresh();
            }
            catch { MessageBox.Show("Er ging iets mis! Inputwaarde verkeerd?!"); }
        }
        private void lbDisplayLk_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            selecteerdeLeerkracht = lbDisplayLk.SelectedItem as Leerkracht;
            if (lbDisplayLk.SelectedItem != null)
            {
                txtLkVoornaam.Text = selecteerdeLeerkracht.Voornaam;
                txtLkNaam.Text = selecteerdeLeerkracht.Naam;
            }
            else
            {
                txtLkVoornaam.Clear();
                txtLkNaam.Clear();
            }
        }

        //Methodes
        private void VeldenInvullen()
        {

            txtLokaalcode.Text = selecteerdeLokaal.LokaalCode;
            dpStart.Value = selecteerdeLokaal.Startuur;
            dpEind.Value = selecteerdeLokaal.Einduur;
        }
        private void ResetVelden()
        {
            txtLokaalcode.Text = "";
            dpStart.Value = null;
            dpEind.Value = null;
        }
        private void LeesAllEnRefresh()
        {

            lijstHuidigeLokalen = Datamanager.GetLokalen();
            lijstHuidigeVakken = Datamanager.GetVakken();
            lijstHuidigeLeerlingen = Datamanager.GetLeerlingen();
            lijstHuidigeLeerkrachten = Datamanager.GetLeerkrachten();
            lbDisplay.ItemsSource = null;
            lbDisplay.ItemsSource = lijstHuidigeLokalen;
            lbDisplayVakken.ItemsSource = null;
            lbDisplayVakken.ItemsSource = lijstHuidigeVakken;
            lbDisplayLln.ItemsSource = null;
            lbDisplayLln.ItemsSource = lijstHuidigeLeerlingen;
            lbDisplayLk.ItemsSource = null;
            lbDisplayLk.ItemsSource = lijstHuidigeLeerkrachten;
            cmbVakken.ItemsSource = null;
            cmbVakken.ItemsSource = lijstHuidigeVakken;
            cmbLeerkracht.ItemsSource = null;
            cmbLeerkracht.ItemsSource = lijstHuidigeLeerkrachten;
            cmbVakkenTwee.ItemsSource = null;
            cmbVakkenTwee.ItemsSource = lijstHuidigeVakken;
        }
    }
}

