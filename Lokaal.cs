//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpeedrunLokaalAny_
{
    using System;
    using System.Collections.Generic;
    
    public partial class Lokaal
    {
        public int LokaalID { get; set; }
        public string LokaalCode { get; set; }
        public Nullable<System.DateTime> Startuur { get; set; }
        public Nullable<System.DateTime> Einduur { get; set; }
        public Nullable<int> FKVak { get; set; }
    
        public virtual Vak Vak { get; set; }
    }
}
