﻿using System.Collections.Generic;
using System.Linq;

namespace SpeedrunLokaalAny_
{
    internal static class Datamanager
    {
        //LOKALEN
        //LOKALEN
        //LOKALEN
        //LOKALEN
        //LOKALEN
        //1 record ophalen (is een obj)
        public static Lokaal GetLokaal(int pkLokaal)
        {

            using (var Database = new DBSpeedrunSchoolEntities())
            {
                var query = from lokaal in Database.Lokalen
                            where lokaal.LokaalID == pkLokaal
                            select lokaal;
                var p = query.FirstOrDefault();                      //Haalt het eerste op dat voldoet aan de voorwaarde (where)
                return p;
            }
        }

        //Alle records ophalen

        public static List<Lokaal> GetLokalen()
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                return Database.Lokalen.ToList();
            }
        }

        //1 wijzigen
        public static bool UpdateLokaal(Lokaal eenLokaal)
        {
            bool updateSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLokaal).State = System.Data.Entity.EntityState.Modified;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }

        //1 verwijderen
        public static bool DeleteLokaal(Lokaal eenLokaal)
        {
            bool deleteSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLokaal).State = System.Data.Entity.EntityState.Deleted;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    deleteSucceeded = true;
                }

                return deleteSucceeded;
            }

        }

        //1 Toevoegen
        public static bool InsertLokaal(Lokaal eenLokaal)
        {
            bool insertSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Lokalen.Add(eenLokaal);
                if (0 < Database.SaveChanges())
                {
                    insertSucceeded = true;
                }
            }
            return insertSucceeded;
        }
        //VAKKEN
        //VAKKEN
        //VAKKEN
        //VAKKEN
        //VAKKEN
        //1 record ophalen (is een obj)
        public static Vak GetVak(int pkVak)
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                var query = from Vak in Database.Vakken
                            where Vak.VakID == pkVak
                            select Vak;
                var p = query.FirstOrDefault();
                return p;
            }
        }

        //Alle records ophalen

        public static List<Vak> GetVakken()
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                return Database.Vakken.ToList();
            }
        }

        //1 wijzigen
        public static bool UpdateVak(Vak eenVak)
        {
            bool updateSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenVak).State = System.Data.Entity.EntityState.Modified;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }

        //1 verwijderen
        public static bool DeleteVak(Vak eenVak)
        {
            bool deleteSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenVak).State = System.Data.Entity.EntityState.Deleted;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    deleteSucceeded = true;
                }

                return deleteSucceeded;
            }

        }

        //1 Toevoegen
        public static bool InsertVak(Vak eenVak)
        {
            bool insertSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Vakken.Add(eenVak);
                if (0 < Database.SaveChanges())
                {
                    insertSucceeded = true;
                }
            }
            return insertSucceeded;
        }
        //LEERLINGEN
        //LEERLINGEN
        //LEERLINGEN
        //1 record ophalen (is een obj)
        public static Leerling GetLeerling(int pkLeerling)
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                var query = from Leerling in Database.Leerlingen
                            where Leerling.LeerlingID == pkLeerling
                            select Leerling;
                var p = query.FirstOrDefault();
                return p;
            }
        }

        //Alle records ophalen

        public static List<Leerling> GetLeerlingen()
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                return Database.Leerlingen.ToList();
            }
        }

        //1 wijzigen
        public static bool UpdateLeerling(Leerling eenLeerling)
        {
            bool updateSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLeerling).State = System.Data.Entity.EntityState.Modified;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }

        //1 verwijderen
        public static bool DeleteLeerling(Leerling eenLeerling)
        {
            bool deleteSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLeerling).State = System.Data.Entity.EntityState.Deleted;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    deleteSucceeded = true;
                }

                return deleteSucceeded;
            }

        }

        //1 Toevoegen
        public static bool InsertLeerling(Leerling eenLeerling)
        {
            bool insertSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Leerlingen.Add(eenLeerling);
                if (0 < Database.SaveChanges())
                {
                    insertSucceeded = true;
                }
            }
            return insertSucceeded;
        }
        //LEERKRACHTEN
        //LEERKRACHTEN
        //LEERKRACHTEN
        //LEERKRACHTEN
        public static Leerkracht GetLeerkracht(int pkLeerkracht)
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                var query = from Leerkracht in Database.Leerkrachten
                            where Leerkracht.LeerkrachtID == pkLeerkracht
                            select Leerkracht;
                var p = query.FirstOrDefault();
                return p;
            }
        }

        //Alle records ophalen

        public static List<Leerkracht> GetLeerkrachten()
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                return Database.Leerkrachten.ToList();
            }
        }

        //1 wijzigen
        public static bool UpdateLeerkracht(Leerkracht eenLeerkracht)
        {
            bool updateSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLeerkracht).State = System.Data.Entity.EntityState.Modified;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }

        //1 verwijderen
        public static bool DeleteLeerkracht(Leerkracht eenLeerkracht)
        {
            bool deleteSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLeerkracht).State = System.Data.Entity.EntityState.Deleted;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    deleteSucceeded = true;
                }

                return deleteSucceeded;
            }

        }

        //1 Toevoegen
        public static bool InsertLeerkracht(Leerkracht eenLeerkracht)
        {
            bool insertSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Leerkrachten.Add(eenLeerkracht);
                if (0 < Database.SaveChanges())
                {
                    insertSucceeded = true;
                }
            }
            return insertSucceeded;
        }









































































        //LeerlingVak
        //LeerlingVak
        //LeerlingVak
        //LeerlingVak


        //Alle records ophalen
        public static List<LeerlingVak> AlleVakkenLeerling(int pkLeerling)
        {
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                var query = from leerlingvak in Database.LeerlingenVakken
                            where leerlingvak.FKLeerling == pkLeerling
                            select leerlingvak;
                var p = query.ToList();
                return p;
            }
        }
        //1 wijzigen
        public static bool UpdateLeerlingVak(LeerlingVak LeerlingVak)
        {
            bool updateSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(LeerlingVak).State = System.Data.Entity.EntityState.Modified;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    updateSucceeded = true;
                }
            }
            return updateSucceeded;
        }

        //1 verwijderen
        public static bool DeleteLeerlingVak(LeerlingVak eenLeerlingVak)
        {
            bool deleteSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.Entry(eenLeerlingVak).State = System.Data.Entity.EntityState.Deleted;
                if (0 < Database.SaveChanges())
                {
                    //Gelukt en opgeslagen
                    deleteSucceeded = true;
                }

                return deleteSucceeded;
            }

        }

        //1 Toevoegen
        public static bool InsertLeerlingVak(LeerlingVak eenLeerlingVak)
        {
            bool insertSucceeded = false;
            using (var Database = new DBSpeedrunSchoolEntities())
            {
                Database.LeerlingenVakken.Add(eenLeerlingVak);
                if (0 < Database.SaveChanges())
                {
                    insertSucceeded = true;
                }
            }
            return insertSucceeded;
        }




































    }
}
