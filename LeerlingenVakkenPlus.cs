﻿using System;

namespace SpeedrunLokaalAny_
{
    public partial class LeerlingVak
    {
        public override string ToString()
        {
            return Datamanager.GetVak(Convert.ToInt32(FKVak)).Naam;
        }
    }
}
