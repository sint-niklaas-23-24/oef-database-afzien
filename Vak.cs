//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SpeedrunLokaalAny_
{
    using System;
    using System.Collections.Generic;
    
    public partial class Vak
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Vak()
        {
            this.LeerlingenVakken = new HashSet<LeerlingVak>();
            this.Lokaal = new HashSet<Lokaal>();
        }
    
        public int VakID { get; set; }
        public string Naam { get; set; }
        public Nullable<int> FKLeerkracht { get; set; }
    
        public virtual Leerkracht Leerkracht { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LeerlingVak> LeerlingenVakken { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Lokaal> Lokaal { get; set; }
    }
}
