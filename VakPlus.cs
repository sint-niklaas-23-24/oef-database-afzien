﻿using System;

namespace SpeedrunLokaalAny_
{
    public partial class Vak
    {
        public override string ToString()
        {

            string bericht;
            if (FKLeerkracht == null)
            {
                bericht = Naam;
            }
            else
            {
                bericht = Naam + " gegeven door: " + Datamanager.GetLeerkracht(Convert.ToInt32(FKLeerkracht)).Voornaam + " " + Datamanager.GetLeerkracht(Convert.ToInt32(FKLeerkracht)).Naam;
            }
            return bericht;
        }
    }
}
