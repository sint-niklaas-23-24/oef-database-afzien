﻿using System;

namespace SpeedrunLokaalAny_
{
    public partial class Lokaal
    {

        public override string ToString()
        {
            string bericht;
            if (FKVak == null)
            {
                bericht = "LokaalID: " + LokaalID + " - Lokaalcode: " + LokaalCode + " - Startuur: " + Startuur + " - Einduur: " + Einduur;
            }
            else
            {
                bericht = "LokaalID: " + LokaalID + " - Lokaalcode: " + LokaalCode + " - Startuur: " + Startuur + " - Einduur: " + Einduur + " - Vak: " + Datamanager.GetVak(Convert.ToInt32(FKVak)).Naam;
            }
            return bericht;
        }

    }
}
