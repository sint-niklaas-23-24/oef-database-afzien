﻿using System.Collections.Generic;

namespace SpeedrunLokaalAny_
{
    public partial class Leerling
    {
        public List<Vak> Vakken
        {
            get; set;
        }
        public override string ToString()
        {
            return Naam + " " + Voornaam;
        }
    }
}
